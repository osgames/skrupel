/**
 * 
 */
package net.doublemalt.skrupel;

import java.io.Serializable;

/**
 * @author DoubleMalt
 *
 */
public class OrbitalSystem implements Serializable
{
    private Long id = null; 
    
    private String name = null;
    
    private int costCantox = 0;
    private int costBaxterium = 0;
    private int costRennurbin = 0;
    private int costVomissan = 0;
    private int costSupplies = 0;
    private int costLemin = 0;

    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }

    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }

    /**
     * @return Returns the name.
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return Returns the costBaxterium.
     */
    public int getCostBaxterium()
    {
        return costBaxterium;
    }

    /**
     * @param costBaxterium The costBaxterium to set.
     */
    public void setCostBaxterium(int costBaxterium)
    {
        this.costBaxterium = costBaxterium;
    }

    /**
     * @return Returns the costCantox.
     */
    public int getCostCantox()
    {
        return costCantox;
    }

    /**
     * @param costCantox The costCantox to set.
     */
    public void setCostCantox(int costCantox)
    {
        this.costCantox = costCantox;
    }

    /**
     * @return Returns the costLemin.
     */
    public int getCostLemin()
    {
        return costLemin;
    }

    /**
     * @param costLemin The costLemin to set.
     */
    public void setCostLemin(int costLemin)
    {
        this.costLemin = costLemin;
    }

    /**
     * @return Returns the costRennurbin.
     */
    public int getCostRennurbin()
    {
        return costRennurbin;
    }

    /**
     * @param costRennurbin The costRennurbin to set.
     */
    public void setCostRennurbin(int costRennurbin)
    {
        this.costRennurbin = costRennurbin;
    }

    /**
     * @return Returns the costSupplies.
     */
    public int getCostSupplies()
    {
        return costSupplies;
    }

    /**
     * @param costSupplies The costSupplies to set.
     */
    public void setCostSupplies(int costSupplies)
    {
        this.costSupplies = costSupplies;
    }

    /**
     * @return Returns the costVomissan.
     */
    public int getCostVomissan()
    {
        return costVomissan;
    }

    /**
     * @param costVomissan The costVomissan to set.
     */
    public void setCostVomissan(int costVomissan)
    {
        this.costVomissan = costVomissan;
    }

    
    
}
