package net.doublemalt.skrupel;

import java.io.Serializable;

public class Attribute implements Serializable
{
    public enum Type
    {
        Lemin,
        Vomissan,
        Rennurbin,
        Baxterium,
        LeminBuried,
        VomissanBuried,
        RennurbinBuried,
        BaxteriumBuried,
        Cantox,
        Supplies,
        Colonists,
        Temperature,
        Owner,
        DomSpeciesType,
        DomSpeciesNum,
        Factories,
        Mines,
        DefenseSystems,
        TechlevelEnergyWeapons,
        TechlevelProjectileWeapons,
        TechlevelHull,
        TechlevelEngines,
        CoordinateX,
        CoordinateY,
        Name,
        Mission,
        Crew,
        Damage,
        Course,
        Speed
    }
    
    private Long id = null;
    private Type type = null;
    private Object value = 0;
    private int round = 0;
    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }
    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }
    /**
     * @return Returns the round.
     */
    public int getRound()
    {
        return round;
    }
    /**
     * @param round The round to set.
     */
    public void setRound(int round)
    {
        this.round = round;
    }
    /**
     * @return Returns the type.
     */
    public Type getType()
    {
        return type;
    }
    /**
     * @param type The type to set.
     */
    public void setType(Type type)
    {
        this.type = type;
    }
    /**
     * @return Returns the value.
     */
    public Object getValue()
    {
        return value;
    }
    /**
     * @param value The value to set.
     */
    public void setValue(Object value)
    {
        this.value = value;
    }

    
}
