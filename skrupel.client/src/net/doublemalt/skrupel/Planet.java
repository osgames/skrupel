/**
 * 
 */
package net.doublemalt.skrupel;

import java.io.Serializable;
import java.util.Vector;

/**
 * @author DoubleMalt
 *
 */
public class Planet implements Serializable
{
    private Long id = null;
    private String name = null;
    private PlanetClass pClass = null;
    private int orbitalPlaces = 0;
    private int coordX = 0;
    private int coordY = 0;
    
    private Vector<Attribute> attributes= new Vector<Attribute>();
    private Vector<OrbitalSystem> orbitalSystems = new Vector<OrbitalSystem>();
    private String log = null;
    /**
     * @return Returns the attributes.
     */
    public Vector<Attribute> getAttributes()
    {
        return attributes;
    }
    /**
     * @param attributes The attributes to set.
     */
    public void setAttributes(Vector<Attribute> attributes)
    {
        this.attributes = attributes;
    }
    /**
     * @return Returns the coordX.
     */
    public int getCoordX()
    {
        return coordX;
    }
    /**
     * @param coordX The coordX to set.
     */
    public void setCoordX(int coordX)
    {
        this.coordX = coordX;
    }
    /**
     * @return Returns the coordY.
     */
    public int getCoordY()
    {
        return coordY;
    }
    /**
     * @param coordY The coordY to set.
     */
    public void setCoordY(int coordY)
    {
        this.coordY = coordY;
    }
    /**
     * @return Returns the id.
     */
    public Long getId()
    {
        return id;
    }
    /**
     * @param id The id to set.
     */
    private void setId(Long id)
    {
        this.id = id;
    }
    /**
     * @return Returns the log.
     */
    public String getLog()
    {
        return log;
    }
    /**
     * @param log The log to set.
     */
    public void setLog(String log)
    {
        this.log = log;
    }
    /**
     * @return Returns the name.
     */
    public String getName()
    {
        return name;
    }
    /**
     * @param name The name to set.
     */
    public void setName(String name)
    {
        this.name = name;
    }
    /**
     * @return Returns the orbitalPlaces.
     */
    public int getOrbitalPlaces()
    {
        return orbitalPlaces;
    }
    /**
     * @param orbitalPlaces The orbitalPlaces to set.
     */
    public void setOrbitalPlaces(int orbitalPlaces)
    {
        this.orbitalPlaces = orbitalPlaces;
    }
    /**
     * @return Returns the orbitalSystems.
     */
    public Vector<OrbitalSystem> getOrbitalSystems()
    {
        return orbitalSystems;
    }
    /**
     * @param orbitalSystems The orbitalSystems to set.
     */
    public void setOrbitalSystems(Vector<OrbitalSystem> orbitalSystems)
    {
        this.orbitalSystems = orbitalSystems;
    }
    /**
     * @return Returns the pClass.
     */
    public PlanetClass getPClass()
    {
        return pClass;
    }
    /**
     * @param class1 The pClass to set.
     */
    public void setPClass(PlanetClass class1)
    {
        pClass = class1;
    }

    

}
